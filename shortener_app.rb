require 'sinatra'
require 'redis'
require 'uri'
require 'sinatra/flash'
require 'json'

enable :sessions

redis = Redis.new

helpers do
  include Rack::Utils
  alias_method :h, :escape_html

  def random_string(length)
    rand(36**length).to_s(36)
  end

  def absolute_url(path)
    "#{request.base_url}/#{path}"
  end

  def invalid_link
    flash[:error] = 'Link format is not valid. Try again? :)'
    redirect '/'
  end
end

get '/' do
  erb :index
end

post '/' do
  url = params[:url]
  return invalid_link unless url =~ URI.regexp
  short_string = random_string(5)
  redis.setnx("links:#{short_string}", url)
  @absolute_url = absolute_url(short_string)
  @stats_url = "#{@absolute_url}/stats"
  erb :index
end

get '/:short_string' do
  short_string = params[:short_string]
  url = redis.get("links:#{short_string}")
  if url
    redis.incr("clicks:#{short_string}")
    redis.sadd("user_data:#{short_string}", request.env.to_json)
  end
  redirect url || '/'
end

get '/:short_string/stats' do
  short_string = params[:short_string]
  @short_url = absolute_url(short_string)
  @url = redis.get("links:#{short_string}")
  @clicks = redis.get("clicks:#{short_string}") || 0
  @user_data = redis.smembers("user_data:#{short_string}").map { |data| JSON.parse(data) }
  erb :stats
end
