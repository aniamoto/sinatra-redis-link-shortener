# README #

Simple Redis+Sinatra link shortener with click counter and visitor stats.

### Setup ###

1. `gem install sinatra sinatra-flash redis json`
2. `$ ruby shortener_app.rb`